/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50636
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2018-06-01 10:36:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_area
-- ----------------------------
DROP TABLE IF EXISTS `tb_area`;
CREATE TABLE `tb_area` (
  `area_id` int(2) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(500) NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tb_area
-- ----------------------------
INSERT INTO `tb_area` VALUES ('1', '东苑', '2', '2018-05-25 00:00:00', '2018-05-26 00:00:00');
INSERT INTO `tb_area` VALUES ('2', '高新区', '1', '2018-05-23 09:46:27', '2018-05-31 09:46:34');
INSERT INTO `tb_area` VALUES ('3', '西苑', '1', null, '2018-06-01 08:51:47');
INSERT INTO `tb_area` VALUES ('4', '南苑', '9', null, null);
INSERT INTO `tb_area` VALUES ('6', '南苑', '9', null, null);
